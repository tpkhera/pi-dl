const Joi = require('@hapi/joi')

const getVideoInfoSchema = Joi.object({
  url: Joi.string()
    .required()
})

const downloadVideoSchema = Joi.object({
  url: Joi.string()
    .required(),
  thumbnail: Joi.string()
    .required(),
  title: Joi.string()
    .required(),
  duration: Joi.alternatives().try(Joi.string(), Joi.number())
    .required(),
  format_id: Joi.string()
    .optional()
})

const removeVideoSchema = Joi.object({
  id: Joi.string()
    .required(),
  deleteFile: Joi.bool()
    .optional()
})

const getVideoStreamSchema = Joi.object({
  id: Joi.string()
    .required(),
  download: Joi.bool()
    .optional()
})

module.exports = {
  getVideoInfoSchema,
  downloadVideoSchema,
  removeVideoSchema,
  getVideoStreamSchema
}
