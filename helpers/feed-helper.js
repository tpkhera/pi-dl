const Parser = require('rss-parser')
const rarbgApi = require('rarbg-api')
const Webtorrent = require('webtorrent')
const DB = require('./db-helper')
const dbHelper = new DB('rarbg.db')
const torrentClient = new Webtorrent()

const fetchRss = async (rssUrl) => {
  const parser = new Parser()
  const feed = await parser.parseURL(rssUrl)
  return feed.items
}

const fetchFeed = async (category) => {
  const feed = await rarbgApi.list({
    category: rarbgApi.CATEGORY[category],
    limit: 100
  })
  return feed
}

const searchFeed = async (searchTerm, category) => {
  const feed = await rarbgApi.search(searchTerm, {
    category: rarbgApi.CATEGORY[category],
    limit: 100
  })
  return feed
}

const matchesSearch = (item, searchTerms) => {
  const title = item.title.toLowerCase().replace(/\./g, ' ')
  const results = []
  searchTerms.forEach(term => {
    if (title.includes(term)) results.push(term)
  })
  return results.length > 0 ? { ...item, searchTerm: results[0] } : 0
}

const appendMetadata = async (items) => {
  for (let index = 0; index < items.length; index++) {
    const element = items[index];
    const torrentLength = await getMagnetMeta(element.link)
    items[index] = { ...element, size: torrentLength }
  }
  torrentClient.destroy()
  return items
}

const getMagnetMeta = (magnetUrl) => new Promise((resolve, reject) => {
  torrentClient.add(magnetUrl, function (torrent) {
    torrent.destroy()
    resolve(torrent.length)
  })
})

const fetchAndStoreTorrentList = async (category, rssUrl, searchTerms) => {
  const rssResult = await fetchRss(rssUrl)
  const filteredResults = rssResult.map((item) => matchesSearch(item, searchTerms)).filter(x => x)
  if (!filteredResults.length) return

  const appendedResults = appendMetadata(filteredResults)

  // FIXME: rarbgApi stopped working
  // const feedResult = await fetchFeed(category)
  // const filteredFeedResults = feedResult.map((item) => matchesSearch(item, searchTerms)).filter(x => x)
  // filteredFeedResults.forEach(item => {
  //   const { title, seeders, leechers } = item
  //   dbHelper.updateDocument({ title }, { $set: { seeders, leechers } })
  // })
  // const feedResultTitles = filteredFeedResults.map(x => x.title)
  // const appendedResults = filteredResults.map(item => {
  //   const { guid, title, link, isoDate, searchTerm } = item
  //   if (feedResultTitles.includes(title)) {
  //     const { seeders, size, leechers, info_page } = filteredFeedResults[feedResultTitles.indexOf(title)]
  //     return { guid, title, link, isoDate, searchTerm, seeders, size, leechers, info_page }
  //   }
  //   return { guid, title, link, isoDate, searchTerm, seeders: 0, size: 0, leechers: 0, info_page: '' }
  // })

  dbHelper.db.ensureIndex({ fieldName: 'guid', unique: true })
  appendedResults.forEach(dbHelper.insertDocument)
  return appendedResults.length
}

module.exports = {
  fetchAndStoreTorrentList
}
