const boom = require('@hapi/boom')

const utilityHelper = require('./utility-helper')

const {
  downloadsCounter,
  getVideoList,
  getVideoInfo,
  downloadVideo,
  getVideoPath,
  validatePayload,
  removeVideo,
  getTorrentList,
  downloadTorrent
} = utilityHelper

const getCurrentDownloadsHandler = async (request, h) => {
  return downloadsCounter.value()
}

const getVideoListHandler = async (request, h) => {
  try {
    const videoList = await getVideoList()
    return videoList
  } catch (error) {
    return boom.internal(error.message)
  }
}

const getVideoInfoHandler = async (request, h) => {
  try {
    validatePayload('GET_VIDEO_INFO', request.payload)
  } catch (err) {
    return boom.badRequest(err.message)
  }

  const { url } = request.payload

  try {
    const videoInfo = await getVideoInfo(url)
    return videoInfo
  } catch (error) {
    return boom.internal(error.message)
  }
}

const downloadVideoHandler = async (request, h) => {
  try {
    validatePayload('DOWNLOAD_VIDEO', request.payload)
  } catch (err) {
    return boom.badRequest(err.message)
  }

  const { url, thumbnail, title, duration, format_id } = request.payload

  try {
    const videoResponse = await downloadVideo({ url, thumbnail, title, duration, format_id })
    return videoResponse
  } catch (error) {
    return boom.internal(error.message)
  }
}

const removeVideoHandler = async (request, h) => {
  try {
    validatePayload('REMOVE_VIDEO', request.payload)
  } catch (err) {
    return boom.badRequest(err.message)
  }

  const { id, deleteFile } = request.payload

  try {
    const removedDocuments = await getVideoPath(id)
    await removeVideo(id, deleteFile)
    return removedDocuments
  } catch (error) {
    return boom.internal(error.message)
  }
}

const getVideoStreamHandler = async (request, h) => {
  try {
    validatePayload('GET_VIDEO_STREAM', request.query)
  } catch (err) {
    return boom.badRequest(err.message)
  }

  const { id, download } = request.query

  try {
    const { filePath, fileName } = await getVideoPath(id)
    if (download) {
      return h.file(filePath, { mode: 'attachment', fileName })
    }
    return h.file(filePath)
  } catch (error) {
    return boom.internal(error.message)
  }
}

const getTorrentListHandler = async (request, h) => {
  try {
    const videoList = await getTorrentList()
    return videoList
  } catch (error) {
    return boom.internal(error.message)
  }
}

const downloadTorrentHandler = (request, h) => {
  try {
    return downloadTorrent(request.payload.url)
  } catch (error) {
    return boom.internal(error.message)
  }
}

module.exports = {
  getVideoListHandler,
  getVideoInfoHandler,
  downloadVideoHandler,
  getVideoStreamHandler,
  getCurrentDownloadsHandler,
  removeVideoHandler,
  getTorrentListHandler,
  downloadTorrentHandler
}
