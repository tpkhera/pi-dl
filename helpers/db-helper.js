const Datastore = require('nedb')
const path = require('path')
const { workingDir } = require('./storage-helper')

class AppDB {
  constructor(dbName) {
    this.db = new Datastore({
      filename: path.join(workingDir, dbName),
      autoload: true,
      timestampData: true
    })
  }

  insertDocument = (document) => new Promise((resolve, reject) => {
    this.db.insert(document, function (err, doc) {
      if (err) reject(err)
      else resolve(doc)
    })
  })

  updateDocument = (filter, document, upsert = false) => new Promise((resolve, reject) => {
    this.db.update(filter, document, { upsert }, function (err, numReplaced, upsert) {
      if (err) reject(err)
      else resolve(numReplaced)
    });
  })

  getDocuments = (filter = {}) => new Promise((resolve, reject) => {
    this.db.find(filter, function (err, docs) {
      if (err) reject(err)
      else resolve(docs)
    })
  })

  removeDocument = (filter, removeMultiple = false) => new Promise((resolve, reject) => {
    this.db.remove(filter, { multi: removeMultiple }, function (err, numRemoved) {
      if (err) reject(err)
      else resolve(numRemoved)
    })
  })
}

module.exports = AppDB
