const fs = require('fs')
const path = require('path')

const appDirs = (function () {
  const WORKING_DIR = 'data'
  const DOWNLOAD_DIR = 'downloads'
  const THUMBS_DIR = 'thumbs'
  const getWorkingDir = () => {
    const workingDir = path.join(process.cwd(), WORKING_DIR)
    try {
      fs.accessSync(workingDir)
    } catch (error) {
      fs.mkdirSync(workingDir)
    }
    return workingDir
  }

  const workingDir = getWorkingDir()

  const getDownloadDir = () => {
    const downloadDir = path.join(workingDir, DOWNLOAD_DIR)
    try {
      fs.accessSync(downloadDir)
    } catch (error) {
      fs.mkdirSync(downloadDir)
    }
    return downloadDir
  }

  const downloadDir = getDownloadDir()

  const getThumbsDir = () => {
    const thumbsDir = path.join(downloadDir, THUMBS_DIR)
    try {
      fs.accessSync(thumbsDir)
    } catch (error) {
      fs.mkdirSync(thumbsDir)
    }
    return thumbsDir
  }

  const thumbsDir = getThumbsDir()
  return {
    workingDir,
    downloadDir,
    thumbsDir
  }
})()

module.exports = appDirs
