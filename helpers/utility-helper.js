const fs = require('fs')
const path = require('path')
const { exec } = require('child_process')
const youtubedl = require('youtube-dl')
const jimp = require('jimp')
const io = require('socket.io')(process.env.SOCKET_IO_PORT)
io.set('transports', ['websocket'])

const { downloadDir, thumbsDir } = require('./storage-helper')
const { rssFeeds: [{torrentAddCmd}] } = require('../rssFetchOpts.json')
const AppDB = require('./db-helper')
const videoDB = new AppDB('data.db')
const validationHelper = require('./validation-helper')

const downloadsCounter = (function () {
  const counter = []

  return {
    add: function (id) {
      counter.push(id)
    },
    remove: function (id) {
      counter.splice(counter.indexOf(id), 1)
    },
    value: function () {
      return counter
    }
  }
})()

const getVideoList = (filter = {}) => {
  return videoDB.getDocuments(filter)
}

const getVideoInfo = (requestUrl) => new Promise((resolve, reject) => {
  youtubedl.getInfo(requestUrl, [], function (err, info) {
    if (err) reject(err)
    resolve(info)
  })
})

const downloadVideo = (requestObject) => new Promise((resolve, reject) => {
  const { url, thumbnail, title, duration, format_id } = requestObject

  console.log('Requested: ' + url)

  let videoInfo = { url, thumbnail, title, duration }

  const videoOptions = format_id ? [`--format=${format_id}`] : []
  const video = youtubedl(url, videoOptions)

  video.on('error', async function (error) {
    const thumbName = await saveThumbnail(thumbnail, videoInfo.id)
    videoDB.insertDocument({
      ...videoInfo,
      thumbnail: thumbName,
      status: 'failed'
    })
    reject(error)
  })

  video.on('info', function (info) {
    console.log('Download started')
    video.pipe(fs.createWriteStream(path.join(downloadDir, `${info.title}.${info.ext}`), { flags: 'w' }))

    videoInfo = {
      ...videoInfo,
      fileName: `${info.title}.${info.ext}`,
      size: info.size,
      id: info.id
    }

    downloadsCounter.add({ [videoInfo.id]: videoInfo })

    resolve(info)
  })

  let pos = 0
  video.on('data', function data (chunk) {
    pos += chunk.length

    if (videoInfo.size) {
      const percent = (pos / videoInfo.size * 100).toFixed(2) - 0.01
      // process.stdout.cursorTo(0)
      // process.stdout.clearLine(1)
      // process.stdout.write(percent + '%')
      io.of(`/${videoInfo.id}`).emit('progress', { status: percent })
    }
  })

  video.on('end', async function () {
    if (videoInfo.size) {
      console.log('\nFinished downloading ' + (videoInfo.size / (1024 * 1024)).toFixed(2) + ' MB')
      const thumbName = await saveThumbnail(thumbnail, videoInfo.id)

      videoDB.insertDocument({
        ...videoInfo,
        thumbnail: thumbName,
        status: 'completed'
      })

      downloadsCounter.remove({ [videoInfo.id]: videoInfo })
      io.of(`/${videoInfo.id}`).emit('progress', { status: 100 })
    }
  })
})

const getVideoPath = async (videoId) => {
  const videoList = await videoDB.getDocuments({ id: videoId })
  const fileName = videoList[0].fileName
  return { filePath: `${downloadDir}/${fileName}`, fileName }
}

const removeVideo = async (videoId, deleteFile) => {
  const removedDocuments = await videoDB.getDocuments({ id: videoId })

  await videoDB.removeDocument({ id: videoId })
  if (deleteFile) {
    fs.unlink(path.join(downloadDir, removedDocuments[0].fileName), (err) => {
      if (err) return err
    })
  }
  return removedDocuments
}

const validatePayload = (type, payload) => {
  let validator
  switch (type) {
    case 'GET_VIDEO_INFO':
      validator = validationHelper.getVideoInfoSchema
      break
    case 'DOWNLOAD_VIDEO':
      validator = validationHelper.downloadVideoSchema
      break
    case 'REMOVE_VIDEO':
      validator = validationHelper.removeVideoSchema
      break
    case 'GET_VIDEO_STREAM':
      validator = validationHelper.getVideoStreamSchema
      break
    default:
      break
  }
  const { value, error } = validator.validate(payload)
  if (error) throw error
  return value
}

const convertImgToBase64URL = (imgUrl) => new Promise((resolve, reject) => {
  const client = (new URL(imgUrl).protocol == 'https:') ? require('https') : require('http')
  client.get(imgUrl, (resp) => {
    resp.setEncoding('base64')
    let body = 'data:' + resp.headers['content-type'] + ';base64,'
    resp.on('data', (data) => { body += data })
    resp.on('end', () => {
      resolve(body)
    })
  }).on('error', (e) => {
    reject(e.message)
  })
})

const saveThumbnail = async (imgUrl, id) => {
  const image = await jimp.read(imgUrl)
  image.resize(150, jimp.AUTO)
  const fileName = `${id}.${image.getExtension()}`
  await image.writeAsync(path.join(thumbsDir, fileName))
  return fileName
}

const getTorrentList = () => {
  const torrentDb = new AppDB('rarbg.db')
  return torrentDb.getDocuments()
}

const downloadTorrent = (url) => new Promise((resolve, reject) => {
  exec(`${torrentAddCmd}"${url}"`, (err, res) => {
    if (err) reject(err)
    resolve(res)
  })
})


module.exports = {
  downloadsCounter,
  getVideoList,
  getVideoInfo,
  downloadVideo,
  getVideoPath,
  removeVideo,
  validatePayload,
  getTorrentList,
  downloadTorrent
}
