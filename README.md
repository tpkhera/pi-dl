# Accio Server

The backend server to [Accio](https://gitlab.com/tpkhera/pi-dl-ui).

### Prerequisites

- [yarn](https://classic.yarnpkg.com/en/docs/install) / [npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)
- [youtube-dl](https://github.com/ytdl-org/youtube-dl)

### Getting started

```bash
git clone https://gitlab.com/tpkhera/pi-dl accio-server
cd accio-server
```
In the `accio-server` directory, create a new `.env` file with the folowing variables
```bash
API_SERVER_HOST=0.0.0.0
API_SERVER_PORT=3001
SOCKET_IO_HOST=0.0.0.0
SOCKET_IO_PORT=3002
ACCIO_USER='accio'
ACCIO_PASS='accio'
ACCIO_COOKIE_PASS='!wsYhFA*C2U6nz=Bu^%A@^F#SF3&kSR6' # should be 32 characters long
```
After setting up `.env`, run
```bash
yarn
yarn start
```

