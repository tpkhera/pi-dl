const fs = require('fs')
const laabr = require('laabr')
const inert = require('@hapi/inert')
const cookie = require('@hapi/cookie')

const options = {
  formats: {
    onPostStart: ':time[iso] :start :level :message',
    response: ':time[iso] :method :remoteAddress :url :status :payload (:responseTime ms)',
    onPostStop: ':time[iso] :stop :level :message'
  },
  tokens: { start: () => '[start]', stop: () => '[stop]' },
  indent: 0,
  stream: fs.createWriteStream('logger.log', { flags: 'a' })
}

const plugins = [{
  plugin: laabr,
  options
}, {
  plugin: inert
}, {
  plugin: cookie
}]

module.exports = plugins
