const Hapi = require('@hapi/hapi')

const appEnv = require('helpers/environment-helper')
const routes = require('./routes')
const plugins = require('./plugins')
const { authOptions } = require('./auth')

const server = Hapi.server({
  port: appEnv.API_SERVER_PORT,
  host: appEnv.API_SERVER_HOST,
  routes: {
    cors: {
      credentials: true
    }
  }
})

const init = async () => {
  try {
    await server.register(plugins)

    server.auth.strategy('session', 'cookie', authOptions)
    server.auth.default('session')

    server.route(routes)
    await server.start()
  } catch (error) {
    console.error(error)
  }
}

module.exports = {
  init
}
