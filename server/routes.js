const requestHelper = require('helpers/request-helper')
const { authHandler } = require('./auth')

const {
  getVideoListHandler,
  getVideoInfoHandler,
  downloadVideoHandler,
  getVideoStreamHandler,
  getCurrentDownloadsHandler,
  removeVideoHandler,
  getTorrentListHandler,
  downloadTorrentHandler
} = requestHelper

const routes = [{
  method: 'GET',
  path: '/hello',
  handler: (request, h) => 'Hello World!',
  options: {
    auth: false
  }
}, {
  method: 'POST',
  path: '/login',
  handler: authHandler,
  options: {
    auth: false
  }
}, {
  method: 'GET',
  path: '/get-video-list',
  handler: getVideoListHandler
}, {
  method: 'POST',
  path: '/get-video-info',
  handler: getVideoInfoHandler
}, {
  method: 'POST',
  path: '/download-video',
  handler: downloadVideoHandler
}, {
  method: 'POST',
  path: '/remove-video',
  handler: removeVideoHandler
}, {
  method: 'GET',
  path: '/get-current-downloads',
  handler: getCurrentDownloadsHandler
}, {
  method: 'GET',
  path: '/get-video-stream',
  handler: getVideoStreamHandler
}, {
  method: 'GET',
  path: '/get-torrent-list',
  handler: getTorrentListHandler
}, {
  method: 'POST',
  path: '/download-torrent',
  handler: downloadTorrentHandler
}, {
  method: 'GET',
  path: '/thumbs/{file*}',
  handler: {
    directory: {
      path: 'data/downloads/thumbs'
    }
  }
}]

module.exports = routes
