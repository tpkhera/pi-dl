const appEnv = require('helpers/environment-helper')
const boom = require('@hapi/boom')

const users = [
  {
    username: appEnv.ACCIO_USER,
    password: appEnv.ACCIO_PASS,
    id: 1
  }
]

const authOptions = {
  cookie: {
    name: 'sid-accio',
    password: appEnv.ACCIO_COOKIE_PASS,
    isSecure: false,
    isSameSite: 'Lax',
    isHttpOnly: false
  },
  validateFunc: async (request, session) => {
    const account = await users.find(user => user.id === session.id)

    if (!account) return { valid: false }

    return { valid: true, credentials: account }
  }
}

const authHandler = async (request, h) => {
  const { username, password } = request.payload
  const account = await users.find(user => user.username === username)

  if (!account || password !== account.password) {
    return boom.unauthorized()
  }

  request.cookieAuth.set({ id: account.id })

  return { status: 'success' }
}

module.exports = {
  authHandler,
  authOptions
}
