FROM node:12.16.0-alpine

WORKDIR /usr/app

COPY package*.json yarn.lock ./

# --no-cache: download package index on-the-fly, no need to cleanup afterwards
# --virtual: bundle packages, remove whole bundle at once, when done
RUN apk --no-cache --virtual build-dependencies add \
    make \
    g++ \
    && yarn \
    && apk del build-dependencies

RUN apk add --no-cache python

COPY . .

EXPOSE 8000 9000

CMD ["yarn", "start"]