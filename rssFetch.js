const { fetchAndStoreTorrentList } =  require('./helpers/feed-helper')
const { rssFeeds } = require('./rssFetchOpts.json')

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

const fetchFeeds = async () => {
  for (let i = 0; i < rssFeeds.length; i++) {
    const { category, rssUrl, searchTerms } = rssFeeds[i]
    await fetchAndStoreTorrentList(category, rssUrl, searchTerms)
    if (i < rssFeeds.length - 1) await sleep(10000)
  }
}

fetchFeeds()